const mong = require('mongoose');

var levelData = new mong.Schema({
	tileset: { type: String, required: true },
	objset: String,
	rows: { type: Number, required: true },
	cols: { type: Number, required: true },
	theme: { type: String, default: "default" }
});

var level = new mong.Schema({
	name: String,
	creator: String,
	description: String,
	tags: Array,
	votes: Number,
	data: levelData
});

level.methods.upvote = function(cb) {
	this.votes += 1;
	this.save(cb);
};
	
mong.model('Level', level);
