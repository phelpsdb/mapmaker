'use strict';
const express = require('express');
const router = express.Router();
const pako = require('pako');
const mong = require('mongoose');
const Level = mong.model('Level');

router.get('/', function(req, res, next) {
	res.sendFile('views/index.html', { root: 'public/dimensions' });
});

router.get('/editor', function(req, res, next) {
	res.sendFile('views/editor.html', { root: 'public/dimensions' });
});

router.param('level', function(req, res, next, id) {
	var mongoQuery = Level.findById(id);
	mongoQuery.exec((err, level) => {
		if (err) {
			console.log('error in exec', err);
			next(err);
		}
		if (!level) {
			console.log('level not found');
			next(new Error('Level not found'));
		}
		req.level = level;
		next();
	});
});

router.get('/game', function(req, res, next) {
	res.sendFile('views/game.html', { root: 'public/dimensions' });
});

router.get('/level/:level', function(req, res) {
	res.json(req.level);
});

router.get('/level', function(req, res, next) {
	Level.find((err, levels) => {
		if (err)
			next(err);
		res.json(levels.map(level => {
			level.id = level._id;
			delete level._id;
			delete level.data;
			return level;
		})).end();
	});
});

router.put('/level/:level/upvote', function(req, res) {
	req.level.upvote( (err, level) => {
		if (err)
			next(err);
		res.json(level);
	});
});

router.post('/level', function(req, res, next) {
	var level = new Level(req.body);
	if (isValidLevel(level)) {
		level.save( (err, level) => {
			if (err) {
				console.log(err);
				next(err);
			}
			res.json(level).end();
		});
	} else {
		console.log('invalid level');
		res.status(400).end();
	}
});

function isValidLevel(level) {
	var result;
	try {
		let objset = pako.inflate(level.data.objset, {to: 'string'});
		result = !(/[^a-zA-Z0-9_\ ]/.test(level.name)) &&
			pako.inflate(level.data.tileset).length === level.data.rows * level.data.cols &&
			isValidJsonString(objset) &&
			hasNoInvalidObjProperties(JSON.parse(objset))
	} catch(err) {
		console.log(err);
		return false;
	}
	return result;
}

function isValidJsonString(jsonString) {
	try {
		JSON.parse(jsonString);
		return true;
	} catch (err) {
		console.log(err);
		return false;
	}
}

function hasNoInvalidObjProperties(objset) {
	for(let key in objset) {
		// TODO
	}
	return true;
}


module.exports = router;
