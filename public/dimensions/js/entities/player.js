'use strict';

function Player(game) {
	this.constraints = {
		runPower: 70,
		runScaler: 0.6, // higher = more slippery
		jumpPower: 450,
		gravity: 1200
	}
	this.game = game; // Phaser game

	this.sprite = game.add.sprite(0, 0, 'player');
	game.physics.arcade.enable(this.sprite);
	this.sprite.body.gravity.y = this.constraints.gravity;

	this.controls = game.input.keyboard.createCursorKeys();
}

Player.prototype.update = function() {
	this.dragVelX(this.constraints.runScaler);
	if (this.controls.left.isDown)
		this.moveLeft();
	if (this.controls.right.isDown)
		this.moveRight();
	if (this.controls.up.isDown && this.sprite.body.touching.down)
		this.jump();
};


Player.prototype.moveLeft = function() {
	this.addVelX(-this.constraints.runPower);
};

Player.prototype.moveRight = function() {
	this.addVelX(this.constraints.runPower);
};

Player.prototype.jump = function() {
	this.addVelY(-this.constraints.jumpPower);
};

Player.prototype.addVelX = function(inc) {
	this.sprite.body.velocity.x += inc;
};

Player.prototype.addVelY = function(inc) {
	this.sprite.body.velocity.y += inc;
};

Player.prototype.dragVelX = function(drag) {
	this.sprite.body.velocity.x *= drag;
};

Player.prototype.resetAt = function(x, y) {
	this.sprite.x = x;
	this.sprite.y = y;
};

