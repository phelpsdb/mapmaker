'use strict';
angular.module('dimensions').service('iconService', ['$config', function($config) {
	this.getIcon = function(icon, extension) {
		return $config.assetsPath + 'editor_icons/' + icon + (extension || '.png');
	}

	this.getGfx = function(image, extension) {
		return $config.assetsPath + 'gfx/' + image + (extension || '.png');
	}

	this.getTile = function(tile, tilesetName) {
		tilesetName = tilesetName || 'base';
		return $config.assetsPath + $config.tilesetPath + tilesetName + '/' + tile + '.png';
	}
}]);
