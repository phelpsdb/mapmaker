'use strict';
(function(app) {
	app.factory('objectElementFactory', [
		'$rootScope',
		'$compile',
		function($rootScope, $compile) {
			return function(width, height, image, bindObj) {
				var newScope = $rootScope.$new(false);
				newScope.bindObj = bindObj;
				var elem = angular.element('<object-element width="'+width+'" height="'+height+'" image="'+image+'">');
				return $compile(elem)(newScope);
			};
		}
	]);

	app.directive('objectElement', ['$config', 'iconService', function($config, ics) {
		return {
			restrict: 'E',
			link: function(scope, elem, attrs) {
				elem.css({
					'position':'absolute',
					'background-image': 'url(' + ics.getIcon(attrs.image) + ')',
					'background-size': 'contain',
					'background-repeat': 'no-repeat',
					'pointer-events': 'auto',
					'width': attrs.width + 'px',
					'height': attrs.height + 'px'
				});

				scope.$watch(function() {return scope.bindObj.x;}, function(newX) {
					elem.css('left', (newX * $config.tilesize) + 'px');
				});

				scope.$watch(function() {return scope.bindObj.y;}, function(newY) {
					elem.css('top', (newY * $config.tilesize) + 'px');
				});
			}
		};
	}]);
})(angular.module('dimensions'))
