'use strict';
angular.module('dimensions').service('gameService', [
	'$http', function($http) {
		this.getLevelList = function() {
			return $http.get('/level');
		};

		this.getLevel = function(levelId) {
			return $http.get('/level/' +levelId);
		};

		this.saveLevel = function(level) {
			return $http.post('/level', level);
		};
	}
]);
