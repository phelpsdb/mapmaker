'use strict';
(function(app) {
	app.controller('editor', [
		'$scope',
		'$http',
		'$config',
		'objectElementFactory',
		'gameService',
		'iconService',
		function($scope, $http, $config, objFac, gameService, icns) {
			var objCanvas = document.getElementById('objCanvas');
			$scope.modal = {};

			$scope.columns = 300;
			$scope.rows = 30;

			$scope.levelMetaData = {
				name: ''
			};
			$scope.tileset = [];
			$scope.objset = {};

			$scope.fileShelf = [
				{
					name: 'save',
					type: 'wgt-simple-btn',
					hint: 'Save this level',
					icon: icns.getIcon('save'),
					clicked: function() {
						createAndShowModal('saveAndPublish', {
								title: 'Save and Publish',
								model: {
									name: ''
								}
							}, [{
								label:'Save and Publish', 
								run: function() {
									$scope.levelMetaData.name = $scope.modal.model.name;
									saveLevel();
								}
							}]
						);
					}
					/*
				}, {
					name: 'saveAs',
					type: 'wgt-simple-btn',
					hint: 'Save this as a new level',
					icon: icns.getIcon('save_as'),
					clicked: function() {
					}
				}, {
					name: 'open',
					type: 'wgt-simple-btn',
					hint: 'Open one of my levels',
					icon: icns.getIcon('file'),
					clicked: function() {
					}
				}, {
					name: 'download',
					type: 'wgt-simple-btn',
					hint: 'Download and edit an existing level',
					icon: icns.getIcon('download'),
					clicked: function() {
					}
					*/
				}
			]

			$scope.toolset = [
				{
					name: 'pencil',
					pretty: 'Pencil',
					icon: icns.getIcon('pencil'),
					hint: 'Click and drag to draw tiles',
					actionOnTile: function(x, y, elem) {
						this.selectedWidget.fillTile(x, y, elem);
					},
					widgets: [
						{
							name: 'solid',
							type: 'wgt-simple-btn',
							icon: icns.getIcon('terrain_solid'),
							hint: 'Click and drag to draw tiles',
							fillTile: function(x, y, elem) {
								elem.style.backgroundColor = 'black';
								$scope.tileset[y][x] = 1;
							}
						}, {
							name: 'spikes',
							type: 'wgt-simple-btn',
							icon: icns.getIcon('terrain_spikes'),
							hint: 'Click and drag to draw spikes',
							fillTile: function(x, y, elem) {
								elem.style.backgroundColor = 'red';
								$scope.tileset[y][x] = 2;
							}
						}
					]
				}, {
					name: 'eraser',
					pretty: 'Eraser',
					icon: icns.getIcon('eraser'),
					hint: 'Click and drag to erase tiles',
					actionOnTile: function(x, y, elem) {
						elem.style.backgroundColor = 'white';
						$scope.tileset[y][x] = 0;
					}
				}, {
					name: 'objectPlacer',
					pretty: 'Object Placement',
					icon: icns.getIcon('start'),
					actionOnTile: function(x, y, elem) {
						this.selectedWidget.place(x, y);
					},
					widgets: [
						{
							name: 'start',
							type: 'wgt-simple-btn',
							icon: icns.getIcon('start'),
							hint: 'Click on a tile to set the player\'s starting position',
							place: function(x, y) {
								if (!$scope.tileset[y][x]) {
									$scope.objset.playerStart.x = x;
									$scope.objset.playerStart.y = y;
								}
							}
						}, {
							name: 'finish',
							type: 'wgt-simple-btn',
							icon: icns.getIcon('finish'),
							place: function(x, y) {
							}
						}
					]
				}
			];

			const mouseStates = {
				down: "down",
				up: "up"
			};
			$scope.mouseState = mouseStates.up;

			$scope.tileMouseOver = function(x, y, e) {
				if ($scope.mouseState === mouseStates.down)
					actionOnTile(x, y, e.target);
			};

			$scope.tileMouseDown = function(x, y, e) {
				$scope.mouseState = mouseStates.down;
				actionOnTile(x, y, e.target);
			};

			$scope.canvasMouseUp = function(e) {
				$scope.mouseState = mouseStates.up;
			};

			$scope.selectTool = function(tool) {
				$scope.selectedTool = tool;
				if(tool.widgets && !tool.selectedWidget) {
					tool.selectedWidget = tool.widgets[0];
				}
				$scope.updateToolHint();
			};

			$scope.selectWidget = function(widget) {
				if($scope.selectedTool.widgets.indexOf(widget) !== -1) {
					$scope.selectedTool.selectedWidget = widget;
				}
			};

			function saveLevel() {
				if (/[^a-zA-Z0-9_\ ]/.test($scope.levelMetaData.name)) {
					showErrorModal('Couldn\'t Save Level', 'Invalid level name');
					return;
				}
				var postLevel = {
					name: $scope.levelMetaData.name,
					data: {
						tileset: pako.deflate(concatTileset($scope.tileset), {to: 'string'}),
						objset: pako.deflate(JSON.stringify($scope.objset), {to: 'string'}),
						rows: $scope.rows,
						cols: $scope.columns
					}
				};
				gameService.saveLevel(postLevel)
					.then(function(data) {
						showErrorModal('Level saved successfully');
					})
					.catch(function(error) {
						showErrorModal('Could not save level', error);
					});
			}

			function initialize() {
				constructTileset();
				$scope.selectTool($scope.toolset[0]);
				$scope.objset.playerStart = { x: 5, y: 7 };
				var playerElem = objFac($config.tilesize, $config.tilesize, 'start', $scope.objset.playerStart);
				objCanvas.appendChild(playerElem[0]);
			}

			function concatTileset(tileset) {
				if (tileset.length === 0)
					return;
				var result = new Uint8Array(tileset.length * tileset[0].length);
				var offset = 0;
				tileset.forEach(function(row) {
					result.set(row, offset);
					offset += row.length;
				});
				console.log('result tileset is: ' + result + ' with length: ' + result.length);
				return result;
			}

			function constructTileset() {
				$scope.tileset = [];
				for (var i = 0; i < $scope.rows; i++) {
					var arr = new Uint8Array($scope.columns); // may there come a day when 256 different tile types is insufficient. -phelpsdb 2016
					for (var j = 0; j < $scope.columns; j++) {
						arr[j] = 0;
					}
					$scope.tileset.push(arr);
				}
			}

			function actionOnTile(x, y, elem) {
				$scope.selectedTool && $scope.selectedTool.actionOnTile(x, y, elem);
			}

			function setPlayerAt(x, y) {
			}

			$scope.updateToolHint = function() {
				var tool = $scope.selectedTool;
				var widget = tool && tool.selectedWidget;
				if (!tool || !tool.hint) 
					$scope.hintText = '';
				else
					$scope.hintText = (widget && widget.hint) || tool.hint;
			};

			$scope.getHintText = function() {
				return $scope.hintText;
			};

			function createAndShowModal(type, options, actions) {
				options = options || {};
				$scope.modal = {};
				for (var opt in options) {
					$scope.modal[opt] = options[opt];
				}
				$scope.modal.title = options.title || '';
				$scope.modal.actions = actions || [];
				$scope.modal.type = type;
				$scope.showModal();
			}
			
			function showErrorModal(title, message) {
				createAndShowModal('error', (message || title) && {
					title: title,
					message: message
				});
			}

			$scope.showModal = function() {
				$scope.modal.visible = true;
			};

			$scope.closeModal = function() {
				$scope.modal.visible = false;
				$scope.modal = {};
			};

			initialize();
		}
	]);
})(angular.module('dimensions'));
