'use strict';

var $game;

(function(app) {
	app.factory('gameFactory', ['$config', function($config) {

		return function(preload, create, update, render, canvasId) {
			return new Phaser.Game($config.width, $config.height, Phaser.AUTO, canvasId, {
				preload: preload,
				create: create,
				update: update,
				render: render
			});
		};
	}]);

	app.controller('dimensionsGame', [
		'$scope',
		'$config',
		'gameFactory',
		'iconService',
		'gameService',
		function($scope, $config, gameFac, ics, gameService) {
			var $game;
			var player;
			var tileGroup;
			var tileMap;
			var levelId;

			$scope.playLevel = function(id) {
				$('#canvasContainer').css('position', 'absolute');
				$scope.levelInSession = true;
				$game = gameFac(pre, create, update, render, 'canvasContainer');
				levelId = id;
			};

			function pre() {
				$game.load.image('tile.base', ics.getTile('solid'));
				$game.load.image('tile.spike', ics.getTile('spikes'));
				$game.load.image('player', ics.getGfx('player_icon'));
				$game.load.image('background', ics.getGfx('background_far_out'));
				$game.time.advancedTiming = true;
			}

			function create() {
				$game.physics.startSystem(Phaser.Physics.ARCADE);

				player = new Player($game);
				tileGroup = $game.add.group();

				$game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
				$game.scale.fullScreenScaleMode = Phaser.ScaleManager.SHOW_ALL;
				loadTileMap(levelId);
				$game.camera.follow(player.sprite, Phaser.Camera.FOLLOW_PLATFORMER);
			}

			function update() {
				if (tileMap) {
					$game.physics.arcade.collide(player.sprite, tileGroup);
					player.update();
				}
			}

			function render() {
				$game.debug.text('fps: ' + $game.time.fps, 20, 20);
			}

			function loadTileMap(id) {
				gameService.getLevel(id).then(function(data) {
					var level = data.data;
					var rawTiles = pako.inflate(level.data.tileset);
					var objset = JSON.parse(pako.inflate(level.data.objset, {to: 'string'}));
					console.log('objset is: ', objset);
					var rows = level.data.rows;
					var cols = level.data.cols;
					tileMap = [];
					for (var i = 0; i < rows; i++) {
						var arr = new Uint8Array(cols);
						for (var j = 0; j < cols; j++) {
							arr[j] = rawTiles[i*level.data.cols + j];
						}
						tileMap.push(arr);
					}
					generateTileSprites(tileMap, tileGroup);
					player.resetAt(objset.playerStart.x * $config.tilesize, objset.playerStart.y * $config.tilesize);
					$game.world.setBounds(0, 0, $config.tilesize * cols, $config.tilesize * rows);

					var background = $game.add.tileSprite(0, 0, $config.tilesize * cols, $config.tilesize * rows, 'background');
					$game.world.sendToBack(background);
				}, function(err) {
					console.log('Error loading level: ', err);
				});
			}

			function generateTileSprites(map, group) {
				const tilesize = 36;
				group.enableBody = true;
				map.forEach(function(row, y) {
					row.forEach(function(tile, x) {
						switch(tile) {
							case 1:
								var tile = group.create(x * $config.tilesize, y * $config.tilesize, 'tile.base');
								tile.body.immovable = true;
								break;
							case 2:
								var tile = group.create(x * $config.tilesize, y * $config.tilesize, 'tile.spike');
								tile.body.immovable = true;
								break;
							default:
								break;
						}
					});
				});
			}
		}
	]);

	app.controller('levelListController', [
		'$scope',
		'gameService',
		function($scope, gameService) {
			$scope.levels = [];

			gameService.getLevelList().success(function(data) {
				$scope.levels = data;
				console.log("got levels ", data);
			});

			$scope.levelClick = function(level) {
				$scope.playLevel(level._id); // defined on parent scope 'dimensionsGame'
			};
		}
	]);

	app.directive('levelList', function() {
		return {
			controller: 'levelListController',
			link: function(scope, elem, cont) {
				console.log('scope, ', scope);
			},
			template: [
				'<button ng-repeat="level in levels" ng-click="levelClick(level)">',
				'	<span class="level-name">{{level.name}}</span>',
				'</button>'
			].join('')
		};
	});

})(angular.module('dimensions'));
