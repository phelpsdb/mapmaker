(function(app) {
	var modalTop = [
		'<div ng-if="modal.visible" id="modalOverlay" ng-click="closeModal()">',
		'	<div id="modal">',
		'		<div id="modalHeader">',
		'			<button id="modalCloseBtn" ng-click="closeModal()">X</button>',
		'			<h2>{{modal.title}}</h2>',
		'		</div>',
		'		<div id="modalBody" ng-bind="modal.body">'
	].join('');

	var modalBottom = [
		'		</div>',
		'		<div id="modalFooter">',
		'			<button ng-repeat="action in modal.actions"',
		'				ng-click="action.run()">{{action.label}}',
		'			</button>',
		'			<button class="cancel" ng-click="closeModal()">',
		'				{{modal.cancelLabel || "Cancel"}}',
		'			</button>',
		'		</div>',
		'	</div>',
		'</div>'
	].join('');

	app.directive('saveModal', function() {
		return {
			restrict: 'C',
			scope: {
				model: '='
			},
			template: [
				modalTop,
				'Name: <input type="text" ng-model="modal.model.name">',
				modalBottom
			].join(''),
			controller: function($scope) {
				$scope.closeModal = function() {
					$scope.modal.visible = false;
				};
			}
		};
	});
})(angular.module('dimensions'));

