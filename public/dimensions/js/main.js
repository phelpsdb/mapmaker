'use strict';

angular.module('dimensions', []).value('$config', {
	tilesize: 36,
	assetsPath: 'dimensions/assets/',
	tilesetPath: 'gfx/tilesets/',
	width: 640,
	height: 400,
	aspect: {
		x: 16,
		y: 10
	}
})
.value('$tileDict', {
	'empty': 0,
	'solid': 1,
	'spike': 2
});
